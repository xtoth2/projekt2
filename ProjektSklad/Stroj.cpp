//
// Created by david on 19. 11. 2019.
//

#include "Stroj.h"

Stroj::Stroj(std::string nazev, int cena, int cenaUskladneni, int strojovyCas)
:Vzor(nazev,cena,cenaUskladneni){
    m_strojovyCas = strojovyCas;
}
int Stroj::getStrojovyCass(){
    return m_strojovyCas;
}
void Stroj::printInfo(){
    std::cout<<"Nazev je: " << m_nazev << std::endl;
    std::cout <<"Cena je: "<< m_cena << std::endl;
    std::cout<<"Cena uskladneni je: " << m_cenaUskladneni << std::endl;
    std::cout<<"Strojovy cas je : " << m_strojovyCas << std::endl;
}
