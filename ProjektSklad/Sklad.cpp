//
// Created by david on 19. 11. 2019.
//

#include "Sklad.h"

Sklad::Sklad(){

}
void Sklad::pridejDrevo(int kolik){
    m_skladDrevo+= kolik;

}
void Sklad::pridejKamen(int kolik){
    m_skladKamene+= kolik;
}
void Sklad::pridejZelezo(int kolik){
    m_skladZelezo+= kolik;
}
void Sklad::pridejFinance(int kolik){
    m_financie+= kolik;
}
void Sklad::pridejStroj(std::string nazev, int cena, int cenaUskladneni, int strojovyCas){
    Stroj*novyStroj = new Stroj(nazev, cena, cenaUskladneni, strojovyCas);
    m_stroje.push_back(novyStroj);
}
void Sklad::pridejVyrobek(std::string nazev, int cena, int cenaUskladneni,
                          int pocetDreva, int pocetZeleza, int pocetKamena, int casVyroby){
    Vyrobek* novyVyrobek = new Vyrobek(nazev, cena, cenaUskladneni, pocetDreva, pocetZeleza, pocetKamena, casVyroby);
    m_vyrobky.push_back(novyVyrobek);

}
void Sklad::odeberDrevo(int kolik){
    m_skladDrevo-= kolik;
}
void Sklad::odeberKamen(int kolik){
    m_skladKamene-= kolik;
}
void Sklad::odeberZelezo(int kolik){
    m_skladZelezo-=kolik;
}
void Sklad::odeberFinance(int kolik){
    m_financie-=kolik;
}
void Sklad::odeberStroj(int kolik){
    if (kolik <= m_stroje.size()){
        m_stroje.erase(m_stroje.begin(),m_stroje.begin()+kolik);
    }
    else{
        std::cout << "Nemas tolko strojov." << std::endl;
    }
}
void Sklad::odeberVyrobek(int kolik){
    if (kolik <=m_vyrobky.size()){
        m_vyrobky.erase(m_vyrobky.begin(),m_vyrobky.begin()+kolik);
    }
}
int Sklad::getDrevo(){
    return m_skladDrevo;
}
int Sklad::getKamen(){
    return  m_skladKamene;
}
int Sklad::getZelezo(){
    return m_skladZelezo;
}
int Sklad::getFinance(){
    return  m_financie;
}
void Sklad::pouziStrojovyCas(int kolik){
    m_maxStrojovyCas-=kolik;
    m_pouzityStrojovyCas+=kolik;
}
int Sklad::getPouzityCas(){
    return m_pouzityStrojovyCas;
}
int Sklad::getStrojovyCas(){
    return m_stroje.size()*100;
}
void Sklad::printInfo() {
    std::cout <<"Stav dreva: "<< getDrevo()<< std::endl;
    std::cout<<"Stav kamena: " << getKamen()<< std::endl;
    std::cout<<"Stav zeleza: " << getZelezo()<< std::endl;

}
int Sklad::getPocetStrojov() {
    return m_stroje.size();
}
