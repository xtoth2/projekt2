//
// Created by david on 19. 11. 2019.
//

#include "Vzor.h"
Vzor::Vzor(std::string nazev, int cena, int cenaUskladneni){
    m_nazev = nazev;
    m_cena = cena;
    m_cenaUskladneni = cenaUskladneni;
}
std ::string Vzor::getNazev(){
    return m_nazev;
}
int Vzor::getCena(){
    return m_cena;
}
int Vzor::getCenaUskladneni(){
    return m_cenaUskladneni;
};
void Vzor::printInfo(){
    std::cout<<"Nazev je: " << m_nazev << std::endl;
    std::cout <<"Cena je: "<< m_cena << std::endl;
    std::cout<<"Cena uskladneni je: " << m_cenaUskladneni << std::endl;
}