//
// Created by david on 19. 11. 2019.
//

#ifndef PROJEKTSKLAD_SKLAD_H
#define PROJEKTSKLAD_SKLAD_H

#include "Vyrobek.h"
#include "Surovina.h"
#include "Stroj.h"
#include <vector>

class Sklad {
    std::vector<Stroj*> m_stroje;
    std::vector<Vyrobek*> m_vyrobky;
    int m_skladDrevo=0;
    int m_skladKamene=0;
    int m_skladZelezo=0;
    int m_financie;
    int m_maxStrojovyCas;
    int m_pouzityStrojovyCas;
public:
    Sklad();
    void pridejDrevo(int kolik);
    void pridejKamen(int kolik);
    void pridejZelezo(int kolik);
    void pridejFinance(int kolik);
    void pridejStroj(std::string nazev, int cena, int cenaUskladneni, int strojovyCas);
    void pridejVyrobek(std::string nazev, int cena, int cenaUskladneni,
                       int pocetDreva, int pocetZeleza, int pocetKamena, int casVyroby);
    void odeberDrevo(int kolik);
    void odeberKamen(int kolik);
    void odeberZelezo(int kolik);
    void odeberFinance(int kolik);
    void odeberStroj(int kolik);
    void odeberVyrobek(int kolik);
    int getDrevo();
    int getKamen();
    int getZelezo();
    int getFinance();
    void pouziStrojovyCas(int kolik);
    int getPouzityCas();
    int getStrojovyCas();
    void printInfo();
    int getPocetStrojov();





};


#endif //PROJEKTSKLAD_SKLAD_H
