//
// Created by david on 19. 11. 2019.
//

#ifndef PROJEKTSKLAD_STROJ_H
#define PROJEKTSKLAD_STROJ_H

#include "Vzor.h"

class Stroj: public Vzor {
    int m_strojovyCas;
public:
    Stroj(std::string nazev, int cena, int cenaUskladneni, int strojovyCas);

    void printInfo();
    int getStrojovyCass();

};


#endif //PROJEKTSKLAD_STROJ_H
