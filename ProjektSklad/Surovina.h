//
// Created by david on 19. 11. 2019.
//

#ifndef PROJEKTSKLAD_SUROVINA_H
#define PROJEKTSKLAD_SUROVINA_H

#include "Vzor.h"
class Surovina: public Vzor {
public:
    Surovina(std::string nazev, int cena, int cenaUskladneni);
    void printInfo();
};


#endif //PROJEKTSKLAD_SUROVINA_H
