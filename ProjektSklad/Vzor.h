//
// Created by david on 19. 11. 2019.
//

#ifndef PROJEKTSKLAD_VZOR_H
#define PROJEKTSKLAD_VZOR_H

#include <iostream>

class Vzor {
protected:
    std::string m_nazev;
    int m_cena;
    int m_cenaUskladneni;
public:
    Vzor(std::string nazev, int cena, int cenaUskladneni);
    std ::string getNazev();
    int getCena();
    int getCenaUskladneni();
    void printInfo();

};


#endif //PROJEKTSKLAD_ZDROJ_H
