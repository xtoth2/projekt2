//
// Created by david on 19. 11. 2019.
//

#ifndef PROJEKTSKLAD_VYROBEK_H
#define PROJEKTSKLAD_VYROBEK_H

#include "Vzor.h"

class Vyrobek: public Vzor {
    int m_pocetDreva;
    int m_pocetZeleza;
    int m_pocetKamena;
    int m_casVyroby;
public:
    Vyrobek(std::string nazev, int cena, int cenaUskladneni,
            int pocetDreva, int pocetZeleza, int pocetKamena, int casVyroby);
    int getPocetDreva();
    int getPocetZeleza();
    int getPocetKamena();
    int getCasVyroby();
    void printInfo();



};


#endif //PROJEKTSKLAD_VYROBEK_H
