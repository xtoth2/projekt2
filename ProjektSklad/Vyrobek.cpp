//
// Created by david on 19. 11. 2019.
//

#include "Vyrobek.h"

Vyrobek::Vyrobek(std::string nazev, int cena, int cenaUskladneni,
int pocetDreva, int pocetZeleza, int pocetKamena, int casVyroby)
:Vzor(nazev, cena, cenaUskladneni){
    m_pocetDreva = pocetDreva;
    m_pocetKamena = pocetKamena;
    m_pocetZeleza = pocetZeleza;
    m_casVyroby = casVyroby;
}
int Vyrobek::getPocetDreva(){
    return m_pocetDreva;
}
int Vyrobek::getPocetZeleza(){
    return m_pocetZeleza;
}
int Vyrobek::getPocetKamena(){
    return m_pocetKamena;
}
int Vyrobek::getCasVyroby(){
    return m_casVyroby;
}
void Vyrobek::printInfo(){
    std::cout<<"Nazev je: " << m_nazev << std::endl;
    std::cout <<"Cena je: "<< m_cena << std::endl;
    std::cout<<"Cena uskladneni je: " << m_cenaUskladneni << std::endl;
    std::cout<<"Pocet dreva je: " << m_pocetDreva << std::endl;
    std::cout<<"Pocet zeleza je: " << m_pocetZeleza << std::endl;
    std::cout<<"Pocet kamena je: " << m_pocetKamena << std::endl;
    std::cout<<"Cas vyroby je: " << m_casVyroby << std::endl;

}
