//
// Created by xtoth2 on 19.11.2019.
//

#include "Student.h"
Student::Student(std::string jmeno, std::string rodneCislo, std::string semestr, float prumer ):Osoba(jmeno, rodneCislo){
    m_semestr = semestr;
    m_prumer = prumer;

}
void Student::setSemestr(std::string semestr){
    m_semestr = semestr;
}
void Student::setPrumer(float prumer){
    m_prumer = prumer;
}
float Student::getPrumer(){
    return m_prumer;
}
std::string Student::getSemestr(){
    return  m_semestr;
}
void Student::printInfo(){
    std::cout <<"Jmeno: "<< m_jmeno<<std::endl;
    std::cout <<"RodneCislo: "<< m_rodneCislo<<std::endl;
    std::cout <<"Semestr: "<< m_semestr<<std::endl;
    std::cout <<"Prumer: "<< m_prumer<<std::endl;

}

