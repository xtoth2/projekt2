//
// Created by xtoth2 on 19.11.2019.
//

#ifndef UNISYSTEM_OSOBA_H
#define UNISYSTEM_OSOBA_H

#include <iostream>

class Osoba {
protected:
    std::string m_jmeno;
    std::string m_rodneCislo;
public:
    Osoba(std::string jmeno, std::string rodneCislo);
    std::string getJmeno();
    std::string getRodneCislo();
    void setJmeno(std::string jmeno);
    void setRodneCilso(std::string rodneCislo);
};


#endif //UNISYSTEM_OSOBA_H
