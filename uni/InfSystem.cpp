//
// Created by xtoth2 on 19.11.2019.
//

#include "InfSystem.h"
InfSystem::InfSystem(){

}
InfSystem::~InfSystem() {
    for (Student *student:m_studenti) {
        delete student;
    }

    for (Ucitel*ucitel:m_ucitele) {
        delete ucitel;
    }

}
void InfSystem::addStudent(std::string jmeno, std::string rodneCislo, std::string semestr, float prumer){
    Student* novyStudent = new Student(jmeno, rodneCislo, semestr, prumer);
    m_studenti.push_back(novyStudent);
}
void InfSystem::addUcitel(std::string jmeno, std::string rodneCislo, std::string ustav){
    Ucitel* novyUcitel = new Ucitel(jmeno, rodneCislo, ustav);
    m_ucitele.push_back(novyUcitel);
}
void InfSystem::printStudenti(){
    for(Student* student:m_studenti){
        student ->printInfo();}
}
void InfSystem::printUcitele(){
    for(Ucitel* ucitel:m_ucitele)
        ucitel ->printInfo();
}


