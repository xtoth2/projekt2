//
// Created by xtoth2 on 19.11.2019.
//

#ifndef UNISYSTEM_INFSYSTEM_H
#define UNISYSTEM_INFSYSTEM_H

#include <vector>
#include "Ucitel.h"
#include "Student.h"


class InfSystem {
    std::vector<Ucitel*> m_ucitele;
    std::vector<Student*> m_studenti;
public:
    InfSystem();
    ~InfSystem();
    void addStudent(std::string jmeno, std::string rodneCislo, std::string semestr, float prumer);
    void addUcitel(std::string jmeno, std::string rodneCislo, std::string ustav);
    void printStudenti();
    void printUcitele();

};


#endif //UNISYSTEM_INFSYSTEM_H
