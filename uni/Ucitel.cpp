//
// Created by xtoth2 on 19.11.2019.
//

#include "Ucitel.h"
Ucitel::Ucitel(std::string jmeno, std::string rodneCislo, std::string ustav):Osoba(jmeno, rodneCislo){
    m_ustav = ustav;

}
void Ucitel::setUstav(std::string ustav){
    m_ustav = ustav;

}
std::string Ucitel::getUstav(){
    return  m_ustav;
}
void Ucitel::printInfo(){
    std::cout <<"Jmeno: "<< m_jmeno<<std::endl;
    std::cout <<"RodneCislo: "<< m_rodneCislo<<std::endl;
    std::cout <<"Ustav: "<< m_ustav<<std::endl;

}