//
// Created by xtoth2 on 19.11.2019.
//

#ifndef UNISYSTEM_UCITEL_H
#define UNISYSTEM_UCITEL_H

#include "Osoba.h"

class Ucitel: public Osoba{
    std::string m_ustav;
public:
    Ucitel(std::string jmeno, std::string rodneCislo, std::string ustav);
    void setUstav(std::string ustav);
    std::string getUstav();
    void printInfo();
};


#endif //UNISYSTEM_UCITEL_H
