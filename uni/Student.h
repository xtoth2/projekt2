//
// Created by xtoth2 on 19.11.2019.
//

#ifndef UNISYSTEM_STUDENT_H
#define UNISYSTEM_STUDENT_H

#include "Osoba.h"

class Student: public Osoba{
    std::string m_semestr;
    float m_prumer;
public:

    Student(std::string jmeno, std::string rodneCislo, std::string semestr, float prumer);
    void setSemestr(std::string semestr);
    void setPrumer(float prumer);
    float getPrumer();
    std::string getSemestr();
    void printInfo();


};


#endif //UNISYSTEM_STUDENT_H
