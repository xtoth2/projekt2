//
// Created by xtoth2 on 10.12.2019.
//

#ifndef TEST_ARMADA_H
#define TEST_ARMADA_H

#include "MobilniJednotka.h"
#include "PesiJednotka.h"
#include <vector>
class Armada {
std::vector<PesiJednotka*> m_pesiJednotka;
std::vector<MobilniJednotka*> m_mobilniJednotka;
public:

    void addPesiJednotka(int nazev, int sila, int obratnost);
    void addMobilniJednotka(int nazev, int sila, int agresivita);

    void vypisArmadu();
    int getSilaArmady();

};


#endif //TEST_ARMADA_H
