//
// Created by xtoth2 on 10.12.2019.
//

#ifndef TEST_BOJOVAJEDNOTKA_H
#define TEST_BOJOVAJEDNOTKA_H

#include <iostream>

class BojovaJednotka {
protected:
    int m_sila;
    int m_nazev;
public:
    BojovaJednotka(int nazev, int sila);
    virtual int getSilaUtoku()=0;
    int getSila();
    int getNazev();
};



#endif //TEST_BOJOVAJEDNOTKA_H
