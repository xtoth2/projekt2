//
// Created by xtoth2 on 10.12.2019.
//

#include "PesiJednotka.h"
PesiJednotka::PesiJednotka(int nazev, int sila, int obratnost):BojovaJednotka(nazev,sila){
    m_obratnost=obratnost;
}
int PesiJednotka::getObratnost(){
    return m_obratnost;
}
int PesiJednotka::getSilaUtoku(){
 int pom;
 pom=PesiJednotka::getSila() * PesiJednotka::getObratnost ();
    return pom;
}
int PesiJednotka::getNazev(){
    return m_nazev;
}
int PesiJednotka::getSila(){
    return m_sila;
}
void PesiJednotka::printInfo() {
    std::cout<<PesiJednotka::getNazev();
}