//
// Created by xtoth2 on 10.12.2019.
//

#ifndef TEST_MOBILNIJEDNOTKA_H
#define TEST_MOBILNIJEDNOTKA_H

#include "BojovaJednotka.h"
#include "Kun.h"

class MobilniJednotka: public BojovaJednotka, Kun {
public:
    MobilniJednotka(int nazev, int sila, int kun);
    int getSilaUtoku();
    int getSila();
    int getNazev();
};


#endif //TEST_MOBILNIJEDNOTKA_H
