//
// Created by xtoth2 on 10.12.2019.
//

#ifndef TEST_PESIJEDNOTKA_H
#define TEST_PESIJEDNOTKA_H

#include "BojovaJednotka.h"

class PesiJednotka: public BojovaJednotka {
int m_obratnost;
public:
    PesiJednotka(int nazev, int sila, int obratnost);
    int getNazev();
    int getSila();
    int getObratnost();
    int getSilaUtoku();
    void printInfo();
};


#endif //TEST_PESIJEDNOTKA_H
