//
// Created by xtoth2 on 10.12.2019.
//

#include "Armada.h"



void Armada::addPesiJednotka(int nazev, int sila, int obratnost) {
    PesiJednotka* novaPesiJednotka = new PesiJednotka(nazev,sila,obratnost);
    m_pesiJednotka.push_back(novaPesiJednotka);
}
void Armada::addMobilniJednotka(int nazev, int sila, int agresivita) {
    MobilniJednotka* novaMobilniJednotka = new MobilniJednotka(nazev,sila,agresivita);
    m_mobilniJednotka.push_back(novaMobilniJednotka);
}
void Armada::vypisArmadu(){
    for (PesiJednotka* pesiJednotka:m_pesiJednotka){
        pesiJednotka->printInfo();
    }
}
int Armada::getSilaArmady(){
    int pomSila;
    int pomSila2;
    for (PesiJednotka* pesiJednotka:m_pesiJednotka){
        pomSila+=pesiJednotka->getSila();
    }
    for (MobilniJednotka* mobilniJednotka:m_mobilniJednotka){
        pomSila2+=mobilniJednotka->getSila();
    }
    return pomSila+pomSila2;
}
