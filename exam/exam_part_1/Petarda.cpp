//
// Created by xkotek on 1/13/2020.
//

#include "Petarda.h"
Petarda::Petarda(int pocetRan, int delka, int minimalniVek, int hlucnost):Ohnostroj(pocetRan,delka,minimalniVek) {
    m_hlucnost = hlucnost;
}
int Petarda::getZabava() {
    return Ohnostroj::getZabava()+ m_hlucnost;
}