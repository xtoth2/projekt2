#include <iostream>
#include "Batoh.h"
#include "Petarda.h"

int main() {
    Batoh *b = new Batoh();
    Ohnostroj *o = new Ohnostroj(5, 10, 18);
    Ohnostroj *o2 = new Ohnostroj(10, 10, 21);
    Ohnostroj *o3 = new Ohnostroj(20, 10, 18);
    Petarda *p1 = new Petarda(20, 10, 18, 500);

    b->pridejOhnostroj(o);
    b->pridejOhnostroj(o2);
    b->pridejOhnostroj(o3);
    b->pridejOhnostroj(p1);
    b->odpalOhnostroj(0);
    std::cout << "Zabava kterou si muze uzit 18 leta osoba je " << b->getZabavaOdVeku(18) << std::endl;
    b->vypisOhnostrojInfo();
    delete (p1);
    delete (o3);
    delete (o2);
    delete (o);
    delete (b);
    return 0;
}