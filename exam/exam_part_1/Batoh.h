//
// Created by xkotek on 1/13/2020.
//

#ifndef EXAM_BATOH_H
#define EXAM_BATOH_H

#include <vector>
#include "Ohnostroj.h"
#include<iostream>

class Batoh {
    std::vector<Ohnostroj *> m_kolekce;
public:
    Batoh();

    void pridejOhnostroj(Ohnostroj *o);

    void odpalOhnostroj(int index);

    int getZabavaOdVeku(int vek);

    void vypisOhnostrojInfo();
};


#endif //EXAM_BATOH_H
