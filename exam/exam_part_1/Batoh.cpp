//
// Created by xkotek on 1/13/2020.
//

#include "Batoh.h"

Batoh::Batoh() {};

void Batoh::pridejOhnostroj(Ohnostroj *o) {
    m_kolekce.push_back(o);
}

int Batoh::getZabavaOdVeku(int vek) {
    int zabava = 0;
    for (int i = 0; i < m_kolekce.size(); i++) {
        if (m_kolekce.at(i)->getVek() <= vek) {
            zabava += m_kolekce.at(i)->getZabava();
        }
    }
    return zabava;
}

void Batoh::odpalOhnostroj(int index) {
    if (m_kolekce.at(index)->getOdpaleno() != 1) {
        m_kolekce.at(index)->setOdpaleno(1);
    } else {
        std::cout << "Bohuzel, ohnostroj na indexu " << index << " je jiz odpalen!" << std::endl;
    }
}

void Batoh::vypisOhnostrojInfo() {
    int odpaleno = 0;
    int pocetZbyvajicichRan = 0;
    std::cout << "Celkovy pocet ohnostroju v batohu: " << m_kolekce.size() << std::endl;
    for (int i = 0; i < m_kolekce.size(); i++) {
        if (m_kolekce.at(i)->getOdpaleno() == 1) odpaleno++;
        if (m_kolekce.at(i)->getOdpaleno() == 0)pocetZbyvajicichRan += m_kolekce.at(i)->getPocetRan();
    }
    std::cout << "Pocet odpalenych ohnostroju v batohu: " << odpaleno << std::endl;
    std::cout << "Pomer odpalenych a neodpalenych ohnostroju v batohu: " << odpaleno << "/" << m_kolekce.size()
              << std::endl;
    std::cout << "Pocet zbyvajicich ran celeho ohnostroje: " << pocetZbyvajicichRan << std::endl;
}