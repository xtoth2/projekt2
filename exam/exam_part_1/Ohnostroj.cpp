//
// Created by xkotek on 1/13/2020.
//

#include "Ohnostroj.h"
Ohnostroj::Ohnostroj(int pocetRan, int delka, int minimalniVek) {
    m_pocetRan = pocetRan;
    m_delka=delka;
    m_minimalniVek=minimalniVek;
    m_odpaleno=0;
}
int Ohnostroj::getZabava() {
    return m_pocetRan*m_delka;
}
bool Ohnostroj::getOdpaleno() {
    return m_odpaleno;
}
void Ohnostroj::setOdpaleno(bool odpaleno) {
    m_odpaleno=odpaleno;
}
int Ohnostroj::getVek() {
    return m_minimalniVek;
}int Ohnostroj::getPocetRan(){
    return m_pocetRan;
}