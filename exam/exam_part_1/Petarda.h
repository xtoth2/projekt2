//
// Created by xkotek on 1/13/2020.
//

#ifndef EXAM_PETARDA_H
#define EXAM_PETARDA_H

#include "Ohnostroj.h"

class Petarda : public Ohnostroj {
    int m_hlucnost;
public:
    Petarda(int pocetRan, int delka, int minimalniVek, int hlucnost);

    int getZabava();
};


#endif //EXAM_PETARDA_H
