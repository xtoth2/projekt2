//
// Created by xkotek on 1/13/2020.
//

#ifndef EXAM_OHNOSTROJ_H
#define EXAM_OHNOSTROJ_H


class Ohnostroj {
    int m_pocetRan;
    int m_delka;
    int m_minimalniVek;
    bool m_odpaleno;
public:
    Ohnostroj(int pocetRan, int delka, int minimalniVek);

    virtual int getZabava();

    bool getOdpaleno();

    void setOdpaleno(bool odpaleno);

    int getVek();

    int getPocetRan();
};


#endif //EXAM_OHNOSTROJ_H
