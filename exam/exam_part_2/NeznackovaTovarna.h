//
// Created by xkotek on 1/13/2020.
//

#ifndef EXAM2_NEZNACKOVATOVARNA_H
#define EXAM2_NEZNACKOVATOVARNA_H

#include "Tovarna.h"

class NeznackovaTovarna : public Tovarna {
public:
    Produkt *vyrobProdukt(ENUMS::typ typProduktu, int nakladyNaVyrobu) override;
};


#endif //EXAM2_NEZNACKOVATOVARNA_H
