//
// Created by xkotek on 1/13/2020.
//

#include "Produkt.h"

Produkt *Produkt::vytvorProdukt(ENUMS::typ typProduktu, int nakladyNaVyrobu, int exkluzivita, bool lowcost) {
    Produkt *novyProdukt = nullptr;
    switch (typProduktu) {
        case ENUMS::typ::BOTY:
            novyProdukt = new Produkt(nakladyNaVyrobu, exkluzivita, lowcost, 800, 100);
            break;
        case ENUMS::typ::TRICKO:
            novyProdukt = new Produkt(nakladyNaVyrobu, exkluzivita, lowcost, 250, 0);
            break;
    }
    return novyProdukt;
}

int Produkt::vypocitejCenu() {
    return m_nakladyNaVyrobu * (m_lowcost * 0.5) + (100 * m_exkluzivita) + m_marze + m_cenaBaleni;
}

Produkt::Produkt(int nakladyNaVyrobu, int exkluzivita, bool lowcost, int marze, int cenaBaleni) {
    m_nakladyNaVyrobu = nakladyNaVyrobu;
    m_exkluzivita = exkluzivita;
    m_lowcost = lowcost;
    m_marze = marze;
    m_cenaBaleni = cenaBaleni;
}