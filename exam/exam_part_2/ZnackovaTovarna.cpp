//
// Created by xkotek on 1/13/2020.
//

#include "ZnackovaTovarna.h"

ZnackovaTovarna::ZnackovaTovarna(int exkluzivita) {
    if (exkluzivita > 0) {
        m_exkluzivita = exkluzivita;
    } else {
        std::cout << "Exkluzivita znackove tovarny musi byt > 0; zadano " << exkluzivita << std::endl;
    }
}

Produkt *ZnackovaTovarna::vyrobProdukt(ENUMS::typ typProduktu, int nakladyNaVyrobu) {
    return Produkt::vytvorProdukt(typProduktu, nakladyNaVyrobu, m_exkluzivita, 0);
}