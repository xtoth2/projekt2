#include <iostream>
#include "NeznackovaTovarna.h"
#include "ZnackovaTovarna.h"

int main() {
    Tovarna *t;
    int x = 0;
    int y = 0;
    while (x != 1 && x != 2) {
        std::cout << "Jakou chcete tovarnu?" << std::endl;
        std::cout << "1 Znackova" << std::endl;
        std::cout << "2 Neznackova" << std::endl;
        std::cin >> x;
    }
    switch (x) {
        case 1:
            while (y < 1) {
                std::cout << "Jakou chcete exkluzivitu tovarny?" << std::endl;
                std::cin >> y;
            }
            t = new ZnackovaTovarna(y);
            break;
        case 2:
            t = new NeznackovaTovarna();
            break;
        default:
            t = new NeznackovaTovarna();
            break;
    }
    x = -1;
    while (x < 0) {
        std::cout << "Kolik penez chcete investovat?" << std::endl;
        std::cin >> x;
    }
    std::cout << "Cena tricka z vyrobnich nakladu o hodnote " << x << " je "
              << t->vyrobProdukt(ENUMS::typ::TRICKO, x)->vypocitejCenu() << std::endl;
    std::cout << "Cena bot z vyrobnich nakladu o hodnote " << x << " je "
              << t->vyrobProdukt(ENUMS::typ::BOTY, x)->vypocitejCenu() << std::endl;
    return 0;
}