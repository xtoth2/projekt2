//
// Created by xkotek on 1/13/2020.
//

#ifndef EXAM2_ZNACKOVATOVARNA_H
#define EXAM2_ZNACKOVATOVARNA_H


#include "Tovarna.h"

class ZnackovaTovarna : public Tovarna {
    int m_exkluzivita;
public:
    Produkt *vyrobProdukt(ENUMS::typ typProduktu, int nakladyNaVyrobu) override;

    ZnackovaTovarna(int exkluzivita);
};


#endif //EXAM2_ZNACKOVATOVARNA_H
