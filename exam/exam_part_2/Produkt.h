//
// Created by xkotek on 1/13/2020.
//

#ifndef EXAM2_PRODUKT_H
#define EXAM2_PRODUKT_H
namespace ENUMS {
    enum class typ {
        BOTY, TRICKO
    };
}

class Produkt {
    int m_nakladyNaVyrobu;
    int m_exkluzivita;
    bool m_lowcost;
    int m_marze;
    int m_cenaBaleni;

    Produkt(int nakladyNaVyrobu, int exkluzivita, bool lowcost, int marze, int cenaBaleni);

public:
    static Produkt *vytvorProdukt(ENUMS::typ typProduktu, int nakladyNaVyrobu, int exkluzivita, bool lowcost);

    int vypocitejCenu();
};


#endif //EXAM2_PRODUKT_H
