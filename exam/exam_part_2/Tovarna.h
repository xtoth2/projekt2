//
// Created by xkotek on 1/13/2020.
//

#ifndef EXAM2_TOVARNA_H
#define EXAM2_TOVARNA_H

#include <iostream>
#include "Produkt.h"

class Tovarna {
public:
    virtual Produkt *vyrobProdukt(ENUMS::typ typProduktu, int nakladyNaVyrobu) = 0;
};


#endif //EXAM2_TOVARNA_H
