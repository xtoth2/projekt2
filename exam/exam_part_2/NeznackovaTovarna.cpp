//
// Created by xkotek on 1/13/2020.
//

#include "NeznackovaTovarna.h"

Produkt *NeznackovaTovarna::vyrobProdukt(ENUMS::typ typProduktu, int nakladyNaVyrobu) {
    return Produkt::vytvorProdukt(typProduktu, nakladyNaVyrobu, 0, 1);
}