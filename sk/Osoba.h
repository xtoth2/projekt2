//
// Created by david on 9. 12. 2019.
//

#ifndef SK_OSOBA_H
#define SK_OSOBA_H

#include <iostream>

class Osoba {
protected:
    std::string m_jmeno;
    std::string m_prijmeni;
    int m_datumNarozeni;
    int m_id;
    std::string m_pohlavi;
public:
    Osoba(std::string jmeno, std::string prijmeni, int datumNarozeni, std::string pohlavi);
    std::string getJmeno();
    std::string getPrijmeni();
    int getDatumNarozeni();
    int getVek();
    std::string getPohlavi();
    int getID();

};


#endif //SK_OSOBA_H
