//
// Created by david on 9. 12. 2019.
//

#ifndef SK_OBCAN_H
#define SK_OBCAN_H

#include <vector>
#include "Osoba.h"

class Obcan: public Osoba {
    std::vector<std::string> m_seznamJazyku;
    std::string m_jazyky;
    static int m_personCount;
    int m_id;
public:
    Obcan(std::string jmeno, std::string prijmeni, int datumNarozeni, std::string pohlavi, std::string jazyky);
    void setJazyk(std::string jazyk);
    std::string getJazyk();
    float getVek();
    void printInfo();
    void naucSeJazyk(std::string novyJazyk);

};


#endif //SK_OBCAN_H
