//
// Created by david on 9. 12. 2019.
//

#ifndef SK_STAT_H
#define SK_STAT_H

#include "Obcan.h"
#include <iostream>
#include <vector>

class Stat {
    std::string m_nazevStatu;
    std::string m_statniJazyk;
    std::vector<Obcan*> m_obcan;
public:
    Stat(std::string nazevStatu);
    float getPocetZen();
    float getPrumVekObcanu();
    void novyObcan(std::string jmeno,std::string prijmeni, int datumNarozeni, std::string pohlavi, std::string jazyky);
    void printObcani();
    void novyObcan(Obcan* novyObcan);
    int pocetMuzu();
};


#endif //SK_STAT_H
