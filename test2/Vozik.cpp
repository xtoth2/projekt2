//
// Created by xkotek on 10.12.2019.
//

#include "Vozik.h"
Vozik::Vozik(float vaha, float rychlost) {
    m_vaha = vaha;
    m_rychlost=rychlost;
}
float Vozik::getRychlost() {
    return m_rychlost;
}
float Vozik::getVaha() {
    return m_vaha;
}