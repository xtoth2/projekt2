//
// Created by xkotek on 10.12.2019.
//

#include "MechanickyVozik.h"
float MechanickyVozik::getRychlostVoziku() {
    int bonus=0;
    if(m_sportovniVarianta)bonus =4;
    return (Vozik::getRychlost()-(Vozik::getVaha()/10))+bonus;
}
MechanickyVozik::MechanickyVozik(bool sportovniVarianta, float vaha, float rychlost):Vozik(vaha,rychlost) {
    m_sportovniVarianta = sportovniVarianta;
}