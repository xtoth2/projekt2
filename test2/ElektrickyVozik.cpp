//
// Created by xkotek on 10.12.2019.
//

#include <iostream>
#include "ElektrickyVozik.h"
ElektrickyVozik::ElektrickyVozik(float zrychleni, float vaha, float rychlost):Vozik(vaha,rychlost) {
    m_zrychleni=zrychleni;
}
float ElektrickyVozik::getRychlostVoziku() {
    return ((m_zrychleni*Vozik::getRychlost())-(Vozik::getVaha()/10));
}