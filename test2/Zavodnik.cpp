//
// Created by xkotek on 10.12.2019.
//

#include "Zavodnik.h"
std::string Zavodnik::getJmeno() {
    return m_jmeno;
}

int Zavodnik::getVek() {
    return m_vek;
}

Zavodnik::Zavodnik(std::string jmeno, int vek) {
    m_jmeno = jmeno;
    m_vek = vek;
    m_vozik = nullptr;
}

float Zavodnik::getRychlostZavodnika() {
    if (m_vozik != nullptr){
        return m_vozik->getRychlostVoziku();
    }
    else
    {
        return m_rychlost;
    }
}
void Zavodnik::vemVozik(Vozik* vozik) {
    m_vozik = vozik;
}