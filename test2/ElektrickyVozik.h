//
// Created by xkotek on 10.12.2019.
//

#ifndef TEST2_ELEKTRICKYVOZIK_H
#define TEST2_ELEKTRICKYVOZIK_H

#include "Vozik.h"
class ElektrickyVozik:public Vozik {
float m_zrychleni;
public:
    float getRychlostVoziku();
    ElektrickyVozik(float zrychleni,float vaha, float rychlost);
};


#endif //TEST2_ELEKTRICKYVOZIK_H
