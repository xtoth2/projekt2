//
// Created by xkotek on 10.12.2019.
//

#ifndef TEST2_VOZIK_H
#define TEST2_VOZIK_H


class Vozik {
float m_rychlost;
float m_vaha;
public:
    Vozik(float vaha, float rychlost);
    virtual float getRychlostVoziku() = 0;
    float getVaha();
    float getRychlost();
};


#endif //TEST2_VOZIK_H
