//
// Created by xkotek on 10.12.2019.
//

#ifndef TEST2_ZAVOD_H
#define TEST2_ZAVOD_H

#include "ElektrickyVozik.h"
#include "Zavodnik.h"
#include <vector>
class Zavod {
std::vector<Zavodnik*> m_seznam;
public:
    void pridejZavodnika(Zavodnik *zavodnik);
    float getPrumernouRychlost();
    void vypisZavodniky();
    Zavod();
};


#endif //TEST2_ZAVOD_H
