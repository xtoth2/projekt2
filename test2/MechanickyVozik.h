//
// Created by xkotek on 10.12.2019.
//

#ifndef TEST2_MECHANICKYVOZIK_H
#define TEST2_MECHANICKYVOZIK_H

#include "Vozik.h"
class MechanickyVozik:public Vozik {
bool m_sportovniVarianta;
public:
    float getRychlostVoziku();
    MechanickyVozik(bool sportovniVarianta,float vaha, float rychlost);
};


#endif //TEST2_MECHANICKYVOZIK_H
