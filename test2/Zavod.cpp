//
// Created by xkotek on 10.12.2019.
//

#include "Zavod.h"
void Zavod::vypisZavodniky() {
    for(int i=0; i< m_seznam.size();i++){
        std::cout <<"Zavodnik "<<i<<": "<<m_seznam.at(i)->getJmeno() <<" "<< m_seznam.at(i)->getVek()<<" "<<m_seznam.at(i)->getRychlostZavodnika()<<"km/h"<<std::endl;
    }
}
float Zavod::getPrumernouRychlost() {
    float soucetRychlosti=0.0f;
    for(int i=0; i< m_seznam.size();i++){
        soucetRychlosti += m_seznam.at(i)->getRychlostZavodnika();
    }
    return soucetRychlosti/m_seznam.size();
}
void Zavod::pridejZavodnika(Zavodnik *zavodnik) {
    m_seznam.push_back(zavodnik);
}
Zavod::Zavod() {
}