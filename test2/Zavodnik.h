//
// Created by xkotek on 10.12.2019.
//

#ifndef TEST2_ZAVODNIK_H
#define TEST2_ZAVODNIK_H

#include <iostream>
#include "Vozik.h"
const float m_rychlost = 3;
class Zavodnik {
 std::string m_jmeno;
 int m_vek;
 Vozik* m_vozik;
public:
    Zavodnik(std::string jmeno, int vek);
    void vemVozik(Vozik* vozik);
    std::string getJmeno();
    int getVek();
    float getRychlostZavodnika();
};


#endif //TEST2_ZAVODNIK_H
