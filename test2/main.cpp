#include <iostream>
#include "Zavodnik.h"
#include "Zavod.h"
#include "ElektrickyVozik.h"
#include "MechanickyVozik.h"
int main() {
    Zavod* zavod = new Zavod();
    ElektrickyVozik* e = new ElektrickyVozik(3,45,7);
    MechanickyVozik* s = new MechanickyVozik(1,25,8);
    MechanickyVozik* m = new MechanickyVozik(0,25,7);
    Zavodnik* z1 = new Zavodnik("Petr",74);
    Zavodnik* z2 = new Zavodnik("Pavel",87);
    Zavodnik* z3 = new Zavodnik("Martin",67);
    Zavodnik* z4 = new Zavodnik("Adam",76);
    zavod->pridejZavodnika(z1);
    zavod->pridejZavodnika(z2);
    zavod->pridejZavodnika(z3);
    zavod->pridejZavodnika(z4);
    z1->vemVozik(e);
    z2->vemVozik(s);
    z3->vemVozik(m);
    zavod->vypisZavodniky();
    std::cout <<zavod->getPrumernouRychlost();
    delete(zavod);
    delete(z1);
    delete(z2);
    delete(z3);
    delete(z4);
    delete(e);
    delete(s);
    delete(m);
    return 0;
}
