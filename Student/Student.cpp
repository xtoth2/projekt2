//
// Created by xtoth2 on 03.12.2019.
//

#include "Student.h"
Student::Student(std::string name, float skolneZaRok, int dobaStudia, bool slevaNaJidlo){
    m_name = name;
    m_skolneZaRok = skolneZaRok;
    m_dobaStudia = dobaStudia;
    m_slevaNaJidlo = slevaNaJidlo;
}
std::string Student::getName(){
    return m_name;
}
float  Student::getSkolneZaRok(){
    return m_skolneZaRok;
}
int  Student::getDobaStudia(){
    return m_dobaStudia;
}
bool  Student::getSlevaNaJidlo(){
    return m_slevaNaJidlo;
}
Student* Student::createStudent(std::string name, StudyType study){
    Student* newStudent = nullptr;
    switch (study){
       case StudyType ::bc:
            newStudent = new Student(name, 1000, 3, true);
            break;
        case StudyType ::mgr:
            newStudent = new Student(name, 2000, 2, true);
            break;
        case StudyType ::phd:
            newStudent = new Student(name, 0, 3, false);
            break;
        default:
            std::cout << "Error, unexpected value" << std::endl;
    }
    return newStudent;
}