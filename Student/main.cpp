#include <iostream>
#include "Student.h"
int main() {
    /*
    Student* bc = new Student("Jan", 1000,3,true);
    Student* mgr = new Student("Pepa",2000,2,true);
    Student* phd = new Student("Joe",0,3,false);
    delete bc;
    delete mgr;
    delete phd;
     */
    Student* bc = Student::createStudent("Jo",StudyType::bc);
    Student* mgr = Student::createStudent("Jea",StudyType::mgr);
    Student* phd = Student::createStudent("Jao",StudyType::phd);
    return 0;
}