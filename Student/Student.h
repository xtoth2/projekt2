//
// Created by xtoth2 on 03.12.2019.
//

#ifndef UNTITLED_STUDENT_H
#define UNTITLED_STUDENT_H

#include <iostream  >
enum class StudyType{
    bc, mgr, phd
};

class Student {
    std::string m_name;
    float m_skolneZaRok;
    int m_dobaStudia;
    bool m_slevaNaJidlo;

    Student(std::string name, float skolneZaRok, int dobaStudia, bool slevaNaJidlo);
public:
    static    Student* createStudent(std::string name, StudyType study);
    std::string getName();
    float getSkolneZaRok();
    int getDobaStudia();
    bool getSlevaNaJidlo();
};


#endif //UNTITLED_STUDENT_H
