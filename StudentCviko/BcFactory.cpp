//
// Created by xtoth2 on 03.12.2019.
//

#include "BcFactory.h"


Student* BcFactory::createStudent(std::string name){
    Student* newStudent = Student::createStudent(name, StudyType:: bc);
    return newStudent;
}
Course* BcFactory::createCourse(std::string name) {
    Course *newCourse = Course::createCourse(name, StudyType::bc);
    return newCourse;
}