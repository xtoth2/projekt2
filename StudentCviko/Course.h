//
// Created by xtoth2 on 03.12.2019.
//

#ifndef UNTITLED_COURSE_H
#define UNTITLED_COURSE_H

#include <iostream  >
enum class StudyType{
    bc, mgr, phd
};
class Course {
    std::string m_name;
    int m_kredits;
    Course(std::string name, int kredits);
public:

    static    Course* createCourse(std::string name, StudyType study);
    std::string getName();
    int getKredits();
};


#endif //UNTITLED_COURSE_H
