//
// Created by xtoth2 on 03.12.2019.
//

#include "Course.h"
Course::Course(std::string name, int kredits) {
    m_name = name;
    m_kredits = kredits;
}
Course* Course::createCourse(std::string name, StudyType study) {
    Course *newCourse = nullptr;
    switch (study) {
        case StudyType::bc:
            newCourse = new Course(name, 20);
            break;
        case StudyType::mgr:
            newCourse = new Course(name, 30);
            break;
        case StudyType::phd:
            newCourse = new Course(name, 40);
            break;
        default:
            std::cout << "Error, unexpected value" << std::endl;
    }
    return newCourse;
}