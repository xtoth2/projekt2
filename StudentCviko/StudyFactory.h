//
// Created by xtoth2 on 03.12.2019.
//

#ifndef UNTITLED_STUDYFACTORY_H
#define UNTITLED_STUDYFACTORY_H

#include <iostream>
#include "Student.h"
#include "Course.h"

class StudyFactory {

    virtual Student* createStudent(std::string name) = 0;
    virtual Course* createCourse(std::string name) = 0;
public:

};


#endif //UNTITLED_STUDYFACTORY_H
