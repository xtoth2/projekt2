//
// Created by xtoth2 on 03.12.2019.
//

#ifndef UNTITLED_MGRFACTORY_H
#define UNTITLED_MGRFACTORY_H
#include <iostream>
#include "StudyFactory.h"
#include "Student.h"

class MgrFactory: public StudyFactory {
    virtual Student* createStudent(std::string name);
    virtual Course* createCourse(std::string name);
};


#endif //UNTITLED_MGRFACTORY_H
