//
// Created by xtoth2 on 03.12.2019.
//

#include "MgrFactory.h"
Student* MgrFactory::createStudent(std::string name) {
    Student *newStudent = Student::createStudent(name, StudyType::mgr);
    return newStudent;
}
Course* MgrFactory::createCourse(std::string name){
    Course* newCourse = Course::createCourse(name, StudyType::mgr);
    return newCourse;
}
