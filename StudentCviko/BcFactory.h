//
// Created by xtoth2 on 03.12.2019.
//

#ifndef UNTITLED_BCFACTORY_H
#define UNTITLED_BCFACTORY_H

#include <iostream>
#include "StudyFactory.h"
#include "Student.h"
class BcFactory: public StudyFactory {
public:
    virtual Student* createStudent(std::string name);
    virtual Course* createCourse(std::string name);
};


#endif //UNTITLED_BCFACTORY_H
